<?PHP
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

$sensor = $_GET["sensor"];
$url    = "http://api.luftdaten.info/v1/sensor/" . $sensor;

$ch = curl_init( $url );
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
curl_setopt( $ch, CURLOPT_HEADER, false );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
$contents = curl_exec( $ch );
curl_close($ch);

header( 'Content-type: application/json' );
print $contents;
?>